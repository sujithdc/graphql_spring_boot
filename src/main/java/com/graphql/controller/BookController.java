package com.graphql.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import com.graphql.entity.Author;
import com.graphql.entity.Book;
import com.graphql.service.AuthorService;
import com.graphql.service.BookService;

@Controller
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@Autowired
	AuthorService authorService;
	
    @QueryMapping
    public Book bookById(@Argument String id) {
        return bookService.getById(id);
    }

    @MutationMapping
    public Book updateBook(@Argument String id, @Argument String name) {
    	return bookService.updateBook(id, name);
    }
    
    @SchemaMapping
    public Author author(Book book) {
        return authorService.getById(book.getAuthorId());
    }
    
    @SchemaMapping(typeName = "Query", value="allBooks")
    public List<Book> getAllBooks() {
    	return bookService.getAllBooks();
    }
}