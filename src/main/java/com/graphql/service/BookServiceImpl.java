package com.graphql.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.graphql.entity.Book;

@Service
public class BookServiceImpl implements BookService{
	
    private static List<Book> books = Arrays.asList(
            new Book("book-1", "Harry Potter and the Philosopher's Stone", 223, "author-1"),
            new Book("book-2", "Moby Dick", 635, "author-2"),
            new Book("book-3", "Interview with the vampire", 371, "author-3")
    );

    public Book getById(String id) {
        return books.stream().filter(book -> book.getId().equals(id)).findFirst().orElse(null);
    }
    
    public List<Book> getAllBooks() {
		return books;
	}
    
	public Book updateBook(String id, String name) {
		System.out.println(name);
		Book book = getById(id);
		if (book != null) {
			book.setName(name);
		}
		System.out.println("**************** "+book.toString());
		return book;
	}

}
