package com.graphql.service;

import com.graphql.entity.Author;

public interface AuthorService {
	public Author getById(String id);
}
