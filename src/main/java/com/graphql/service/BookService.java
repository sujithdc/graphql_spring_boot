package com.graphql.service;

import java.util.List;
import com.graphql.entity.Book;

public interface BookService {

	 public Book getById(String id);
	 public List<Book> getAllBooks();
	 public Book updateBook(String id, String title);
	 
}
