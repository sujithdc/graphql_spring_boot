package com.graphql.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.graphql.entity.Author;

@Service
public class AuthorServiceImpl implements AuthorService{

	 private static List<Author> authors = Arrays.asList(
	            new Author("author-1", "Joanne", "Rowling"),
	            new Author("author-2", "Herman", "Melville"),
	            new Author("author-3", "Anne", "Rice")
	    );

	    public Author getById(String id) {
	        return authors.stream().filter(author -> author.getId().equals(id)).findFirst().orElse(null);
	    }

}
